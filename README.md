# Robot Manipulator Structural

This repository contains 3D models for an underwater robot manipulator that used in [CS Laboratory-NTUA](http://www.controlsystemslab.gr/main/).

Some requirements:

* Mass less than 600g
* The robot arm should be able to reach the bottom of the water tank with the AUVs hovering at a minimum altitude of about 30 cm.
* Have a pinhole camera in the center of EE, with 25 frame at second (at least). This camera must be located so as not seeing the end effector‘s finger (clear view), look at end effectors folder
* Have a force-torque sensor (is a 6 axis possible?) on the EE for cooperative manipulation tasks
* Have 4 DOF
* Attached close to the center of the AUV
* Have for each part neutrally buoyant to reduce dynamic disturbances on the vehicle

It is important to emphasize that the underwater vehicles are:
* [VideoRay pro 3](http://www.videoray.com/)
* [Seabotix LBV 150SE](http://www.teledynemarine.com/lbv150-4)

A critical note is that all the process isn't well document due to limited time of development.

![arm_construction](pics/arm-construction-1.jpg)

# Specification

### Manipulator Technical specification:

| | |
| --- | --- |
| Degrees of freedom | 4 |
| Mass in air | ~950 gr |
| Payload in air | ~200 gr |
| Reach (W/O end effector) | 31mm |
| Repeatability | ? |
| Power Supply | 6V@15A |
| Communication | RS485 |

### D-H Parameters:

According to D-H parameter for [Robotics-Toolbox](http://petercorke.com/wordpress/toolboxes/robotics-toolbox)

```matlab
function [A, J, RobotArm] = RobotArm_kin(q)

q = deg2rad(q);

% lengths of robot arm in mm
L1 = 77.8;
L2 = 2.2;
L3 = 147.69;
L4 = 28;
L5 = 75.4;

% dh = [THETA D A ALPHA SIGMA OFFSET]
Link0 = Link([0 L1 L2 -pi/2 0 0], 'standard');
Link1 = Link([0 0 L3 0 0 -pi/2], 'standard');
Link2 = Link([0 0 L4 -pi/2 0 -pi/2], 'standard');
Link3 = Link([0 L5 0 0 0 0], 'standard');

% robot arm
RobotArm = SerialLink([Link0 Link1 Link2 Link3], 'name', 'Arm');

A = RobotArm.fkine(q(1:4));
J = RobotArm.jacob0(q(1:4));
RobotArm.plot(q(1:4));
```
![d-h-parameters](pics/d-h-parameters.png)

| Link | THETA | D | A | ALPHA | SIGMA | OFFSET |
| --- | --- | --- | --- | --- | --- | --- |
| Link0 | 0 | L1 | L2 | -pi/2 | 0 | 0 |
| Link1 | 0 | 0 | L3 | 0 | 0 | -pi/2 |
| Link2 | 0 | 0 | L4 | -pi/2 | 0 | -pi/2 |
| Link3 | 0 | L5 | 0 | 0 | 0 | 0 |

Also here is the scripts for python [KDL](http://wiki.ros.org/kdl/Tutorials/Frame%20transformations%20%28Python%29):

```python
from PyKDL import *
import numpy as np

# ^ z
# |
# |
# x --->x
chain = Chain()

joint0 = Joint(Joint.RotZ)
frame0 = Frame(Vector(2.2, 0.0, 32.8))
segment0 = Segment(joint0, frame0)
chain.addSegment(segment0)

joint1 = Joint(Joint.RotY)
frame1 = Frame(Vector(0.0, 0.0, 147.7))
segment1 = Segment(joint1, frame1)
chain.addSegment(segment1)

joint2 = Joint(Joint.RotY)
frame2 = Frame(Vector(28.0, 0.0, 75.4))
segment2 = Segment(joint2, frame2)
chain.addSegment(segment2)

joint3 = Joint(Joint.RotZ)
frame3 = Frame(Vector(0.0, 0.0, 0.4))
segment3 = Segment(joint3, frame3)
chain.addSegment(segment3)

def forwardKinematics(q):
    jointAngles = JntArray(4)
    jointAngles[0] = q[0]
    jointAngles[1] = q[1]
    jointAngles[2] = q[2]
    jointAngles[3] = q[3]

    fk = ChainFkSolverPos_recursive(chain)
    finalFrame = Frame()
    fk.JntToCart(jointAngles, finalFrame)

    return finalFrame

def calcJacobian(q):
    jointAngles = JntArray(4)
    jointAngles[0] = q[0]
    jointAngles[1] = q[1]
    jointAngles[2] = q[2]
    jointAngles[3] = q[3]

    jacobian = Jacobian(4)
    solver = ChainJntToJacSolver(chain)
    solver.JntToJac(jointAngles, jacobian)

    return jacobian

def inverseKinematics(q, p):
    q_init = JntArray(4)
    q_init[0] = q[0]
    q_init[1] = q[1]
    q_init[2] = q[2]
    q_init[3] = q[3]

    fk = ChainFkSolverPos_recursive(chain)
    vik = ChainIkSolverVel_pinv(chain)
    ik = ChainIkSolverPos_NR(chain, fk, vik)
    desiredFrame = Frame(Vector(p[0], p[1], p[2]))

    q_out = JntArray(3)
    ik.CartToJnt(q_init, desiredFrame, q_out)

    return q_out
```

### Actuators Specification:

The actuator, [SAVOX SW-0231MG](https://www.savox-servo.com/Servos/Waterproof/Savox-Servo-SW-0231MG-Digital-DC-Motor-Waterproof-Metal-Gear/), uses [SuperModified v3.0](https://www.01mechatronics.com/product/supermodified-v30-dc-motors) electronics control board instead of the default electronics of servo and here is the [datasheet](http://www.01mechatronics.com/sites/default/files/docs/SuperModified.V3.Datasheet.pdf).

| | |
| --- | --- |
| Nominal Voltage (V)  | 6 |
| Stall Current (A)  | 4.8 |
| Stall Torque (Nm) | 1.47 |
| Nominal Torque (Nm) | ~0.8 | 
| Rated speed at no load (deg/s) | ~353 |
| Running current at no load (mA) | ~150 |
| Idle Current (mA) | 20 |
| Weight (g) | 75 |
| Counts per Revolution | 15bit |
| Dust / Water Resistance | IP67 |

Also according to [this](https://www.mathworks.com/matlabcentral/fileexchange/54695-polulu-motor-plot-generator), it generates the below performance graph of the servo motor.

![sw0231-perfgraph](pics/sw0231-perfgraph.png)

### Joint Specifications:

According to zero position as defined from D-H parameters (frames). In above table represented the specification of each joint. The velocity is the efficiency velocity according to performance graph of servo motor considering the gear ratio. Also for torque, is the efficiency torque according to performance graph considering the gear ratio.

| Axis Data | Range | Velocity (deg/s) | Torque (Nm) | Gear Ratio |
| ---      | ---       | ---       | ---       | ---       |
| Joint1 | -180/180 | 90 | 1.6  | 2 |
| Joint2 | -68.7/101.5 | 80 | 1.8 | 2.25 |
| Joint3 | -92.4/139.3 | 80 | 1.8 | 2.25 |
| Joint4 | -180/180 | 180 | 0.8 | 1 |


### Links Specification

According to frames that introduced in D-H parameters and as exported from analysis in CAD.
Also to calculate the "wet weight", tips are used from [this report](http://www.cohodesignsllc.com/wp-content/uploads/2012/08/Whitepaper-wet-weight_no-title.pdf).

* Center of Buoyancy in each Link

| Joint | X (mm) | Y (mm) | Z (mm) | 
| --- | --- | --- | --- |
| 1 | 13.19 | 0.77 | 44.1 |
| 2 | -0.02 | 68.09 | -8.2 |
| 3 | 39.69 | 17.98 | -6.54 |

* Center of Gravity in each Link

| Joint | X (mm) | Y (mm) | Z (mm) |
| --- | --- | --- | --- |
| 1 | 15.14 | 0.43 | 40.85 |
| 2 | -0.05 | 70.51 | -5.43 |
| 3 | 35.09 | 14.7 | -5.07 |

![arm-construction-2](pics/arm-construction-2.jpg)

* Volume of each Link

| Link | Volume (mm^3) |
| --- | --- |
| 1 | 140429.23 |
| 2  | 252157.40 |
| 3 | 108622.63 |

* Wet Weight

| Link | Force (g) | Sink or Float |
| --- | --- | --- |
| 1 | 173.69 - 156.17 = 17.52 | Float |
| 2 | 357.08 - 292.01 = 65.07 | Float |
| 3 | 141.88 - 109.63 = 32.25 | Float |

# Software

The ROS-package for each servo-motor of manipulator is placed [here](https://gitlab.com/zisi/SuperModifiedServo-ROS). In order to initialize the robot manipulator either the manipulator could start from zero position as appeared from D-H parameters or the manipulator could find the zero position by detect a collision by measuring the load of servo motor.
For example in joints 2 and 3 the offset angles and collision loads are
appear in below table. The unit of collision load is as return from function [uint16_t getCurrent(int fd, uint8_t nodeId)](https://gitlab.com/zisi/SuperModifiedServo-ROS/blob/master/src/ZerooneSupermodified.c#L639), which is a digital value.

| Joint| Zero Offset (deg) | Collision Load |
| --- | --- | --- |
| 2 | -68.7 | 2500 |
| 3 | -139.3 | 2500 |

# Testing - Experiments

The manipulator is tested under the water and also about the repeatability. The manipulator is used in experiment with under water vehicles as shown in below videos.

* [Various Tests]( https://youtu.be/kZCDYzImzsI)
* [A Decentralized Predictive Control Approach for Cooperative, Shahab Heshmati-alamdari](https://www.youtube.com/watch?v=noRZNsjX6N0&feature=youtu.be)
* [PHD Thesis, Shahab Heshmati-alamdari](https://shahabheshmati.github.io/Heshmati_PhDthesis.pdf)

### Various End-Effectors

![ee-2](pics/ee-2.png) 
![ee-1](pics/ee-1.jpg)

## License

Licensed under the [CERN OHLv1.2.](https://gitlab.com/zisi/robot-manipulator-structural/blob/csl/LICENSE)
